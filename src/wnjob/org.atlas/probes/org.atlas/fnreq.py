#! /usr/bin/python3
import sys
import subprocess

#get list of squids and frontiers
fserver = subprocess.check_output("echo $FRONTIER_SERVER", shell=True).decode(sys.stdout.encoding)
fserverParts = []
for fs in fserver.split(')'):
  fserverParts.append(fs+')')
#print(fserverParts)

frontiers = ''
for fsp in fserverParts:
  if 'serverurl' in fsp:
    frontiers += fsp
#print(frontiers)

squids = []
for fsp in fserverParts:
  if 'proxyurl' in fsp:
    squids.append(fsp)
#print(squids)

#get path to fn-req
fpath = ''
output = subprocess.check_output("echo $LD_LIBRARY_PATH", shell=True).decode(sys.stdout.encoding)
for i in output.split(':'):
  if 'frontier' in i:
    fpath = i[:-3]
c = 0

#loop over each squid and do the query
for s in squids:
  print('')
  print('Proxy:'+s)
  fcommand = "echo 'select 1 from dual'"+chr(124)+"FRONTIER_LOG_LEVEL=warning FRONTIER_SERVER="+'"'+frontiers+s+'" '+fpath+"bin/fn-req"
  fnout = subprocess.check_output(fcommand, shell=True).decode(sys.stdout.encoding)
  if 'warn' in fnout or 'error' in fnout:
    print("Command used:\n"+fcommand)
    print("")
    print("Output:\n"+fnout)
    c += 1

#print the results
print('')
if c == 0:
  print("OK: test passed\n")
  sys.exit(0) #check the exit code: 0=OK
elif c == len(squids):
  print("  -- ERROR: none of the Proxy/ServerURL combinations works \n\n")
  sys.exit(20) #20 ==ERROR
else:
  print("WARNING: some Proxy/ServerURL combinations don't work\n")
  sys.exit(10) #10 ==WARNING (at least one combination worked)
