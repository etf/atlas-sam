#!/bin/bash

source /opt/omd/sites/etf/.oidc-agent/oidc-env.sh
cp -f /etc/grid-security/tokens/* /opt/omd/sites/etf/.oidc-agent/
oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/etf_atlas_ce.key etf_atlas_ce
/usr/lib64/nagios/plugins/refresh_token -t 7200 --token-config etf-atlas-prod --token-time 345600 --aud /var/lib/gridprobes/atlas.Role.lcgadmin/scondor/




