#!/bin/bash

source /usr/bin/etf-init.sh

cat << "EOF"
 _____ _____ _____      _  _____ _        _    ____
| ____|_   _|  ___|    / \|_   _| |      / \  / ___|
|  _|   | | | |_      / _ \ | | | |     / _ \ \___ \
| |___  | | |  _|    / ___ \| | | |___ / ___ \ ___) |
|_____| |_| |_|     /_/   \_|_| |_____/_/   \_|____/
=====================================================
EOF

print_header

etf_update

start_xinetd

etf_init

copy_certs

apache_init

config_web_access

config_alerts

init_api

config_htcondor

config_etf

fix_cmk_theme

etf_start

fix_live_ip6

start_oidc_agent

echo "Fetching ATLAS credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy -b 2048 --vo-fqan /atlas/Role=lcgadmin --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo atlas --lifetime 72 --name NagiosRetrieve-ETF-atlas -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--atlas-Role_lcgadmin --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy -b 2048 --vo-fqan /atlas/Role=production --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo atlas --lifetime 72 --name NagiosRetrieve-ETF-atlas -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--atlas-Role_production --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

echo "Initialising tokens ..."
su - etf -c "oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/etf_atlas_ce.key etf_atlas_ce"
su etf -c "/usr/lib64/nagios/plugins/refresh_token -t 7200 --token-config etf-atlas-prod --token-time 345600 --aud /var/lib/gridprobes/atlas.Role.lcgadmin/scondor/"

echo "Running ATLAS cron jobs ..."
bash /etc/cron.daily/getCRICATLASInfo.sh

etf_wait