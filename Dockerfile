FROM gitlab-registry.cern.ch/etf/docker/etf-base:el9

COPY ./docker/config/ce-client.conf /etc/condor-ce/config.d/ce-client.conf

# Install gfal
RUN yum -y install python3-gfal2 python3-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp gfal2-plugin-xrootd

# Xroot
RUN yum -y install xrootd-client

# SRM
RUN yum -y install python3-gfal2 python3-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp

# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
RUN echo "joblisttype=XML" > /opt/omd/sites/$CHECK_MK_SITE/.arc/client.conf
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY docker/config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile
# IAM VOMSES
RUN mkdir -p /etc/vomses
COPY docker/config/voms-atlas-auth.app.cern.ch.vomses /etc/vomses


# ETF WN-qFM payload
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/bin
RUN cp /usr/bin/etf_wnfm /usr/libexec/grid-monitoring/wnfm/bin/
RUN cp -r /usr/lib/python3.9/site-packages/pexpect /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python3.9/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python3.9/site-packages/wnfm /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages

# ETF streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
COPY docker/config/ocsp_handler.cfg /etc/nstream/

# ATLAS config and payload
COPY extras/vofeed_atlas.py /usr/lib/ncgx/x_plugins/
COPY extras/getCRICATLASInfo.sh /etc/cron.daily/
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.atlas/wnjob/
COPY src/wnjob /usr/libexec/grid-monitoring/probes/org.atlas/wnjob
COPY src/DDM /usr/libexec/grid-monitoring/probes/org.atlas/DDM
COPY extras/getCRICATLASInfo.py /usr/libexec/grid-monitoring/probes/org.atlas/
# ATLAS DDM probe logrotate
COPY extras/gfal2_logs /etc/logrotate.d/
RUN chmod 0644 /etc/logrotate.d/gfal2_logs

# ETF config
COPY extras/wlcg_atlas.cfg /etc/ncgx/metrics.d/
COPY docker/config/atlas_checks.cfg /etc/ncgx/conf.d/
COPY docker/config/ncgx.cfg /etc/ncgx/

COPY ./docker/add-keys.sh /opt/omd/sites/etf/.oidc-agent/

EXPOSE 443 6557
ENTRYPOINT ["/usr/sbin/init"]